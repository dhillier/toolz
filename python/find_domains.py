"""Dirty script for roughly scraping domain names out of files with select extensions.
The output will be a list of domain names based on emails found or URL found.
"""

import re
from pathlib import Path

email_re = re.compile(r'.*[^\s"\'\{\}]+@{1}(?P<domain>[a-zA-Z0-9_\-]+\.[^\s^\>^\\^\/^\'^\"^:\)^,]+)')
url_re = re.compile(r'.*:\/\/(?P<domain>[^\/^\\^\"^\'^\>^:^\)^,]+)')


def main():
    extensions = ['.py', '.html', '.js', '.json']
    exclusions = ['node_modules', '__pycache__', ]
    base_dir = Path('deferit')
    email_domains = set()
    url_domains = set()

    for path in base_dir.rglob(r'**/*'):
        excluded = any([x for x in exclusions if x in path.name])
        if not excluded and not path.is_dir() and path.suffix in extensions:
            try:
                with open(path, 'r') as f:
                    content = f.readlines()
                    for line in content:
                        m_eml = email_re.match(line)
                        
                        if m_eml:
                            email_domains.add(m_eml.group('domain'))
                            #print('{} => {} : {}'.format(path.name, m_eml.group('domain'), line))

                        m_url = url_re.match(line)
                        if m_url:
                            url_domains.add(m_url.group('domain'))
                            #print('{} => {} : {}'.format(path.name, m_url.group('domain'), line))
            except KeyboardInterrupt:
                break
            except Exception as err:
                print(f'{err} File: {path}')


    print('Email Domains:')
    for ed in sorted(email_domains):
        print('{}{}'.format('    --->' if 'defer' in ed else '\t', ed))

    print('URL Domains:')
    for ed in sorted(url_domains):
        print('{}{}'.format('    --->' if 'defer' in ed else '\t', ed))

if __name__ == "__main__":
    main()